package Strings;

import java.util.HashMap;
import java.util.Map;

public class RepeatedWords {

    public static void main(String[] args) {
         String str= "Java is language,Java  good language";
         System.out.println(repeatWord(str));
    }

    public static int repeatWord(String sentence){

        Map<String,Integer> map = new HashMap<>();

        String [] str = sentence.toLowerCase().split("\\W+");

        for(String s : str){
            map.put(s,map.getOrDefault(s,0)+1);
        }

        int repeatedCount=0;

        for(int i : map.values()){
            if(i>1) {
                repeatedCount++;
            }
        }
        return repeatedCount;
    }
}

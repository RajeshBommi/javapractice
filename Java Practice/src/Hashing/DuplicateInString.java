package Hashing;

import java.util.HashMap;
import java.util.Map;

public class DuplicateInString {

    public static void main(String[] args) {

        HashMap<Character,Integer> map = new HashMap<>();

        String str = "Lollypop";

        for(char c : str.toCharArray()){
            map.put(c,map.getOrDefault(c,0)+1);
        }
        for(Map.Entry<Character,Integer> m : map.entrySet()){
            if(m.getValue()>1){
                System.out.println(m.getKey());
            }
        }
    }
}

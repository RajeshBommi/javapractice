package Stack;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ReverseString {

    public static void main(String[] args) {

        String str="Rajesh";
//        int [] arr= {1,2,3,4,5};
        Stack<Character> stack1=new Stack<>();
//        Stack<Character> stack2=new Stack<>();
//        List<Character> list = new ArrayList<>();
        String reverse ="";

        for(int i=0;i<str.length();i++){
            char c= str.charAt(i);
            stack1.push(c);
        }
        System.out.println(stack1);

        while(!stack1.isEmpty()) {
//            list.add(stack1.pop());
//            stack2.push(stack1.pop());
            char c=stack1.pop();
            reverse+=c;
        }
        System.out.println(reverse);

//        while(!stack1.isEmpty()){
//            stack2.push(stack1.pop());
//        }
//        StringBuilder reversedString = new StringBuilder();
//        while (!stack1.isEmpty()){
//            reversedString.append(stack1.pop());
//        }
//        System.out.println(reversedString);
    }
}

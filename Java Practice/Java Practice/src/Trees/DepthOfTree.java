package Trees;

//import javax.swing.tree.TreeNode;
import java.util.Scanner;

public class DepthOfTree {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the root value");
        int root = sc.nextInt();
        CreateNode rootVal= new CreateNode(root);
        addChild(rootVal,sc);
        System.out.println("Max Depth of the tree is:"+maxDepth(rootVal));

    }
    public static void addChild(CreateNode node , Scanner sc){
        System.out.println("Do you want to enter the left child for root value"+ node.val+"Y/N??");
        char choice=sc.next().charAt(0);
        if(choice=='Y'){
            System.out.println("Enter the left child value");
            int left=sc.nextInt();
            node.left=new CreateNode(left);
            addChild(node.left,sc);
        }
        System.out.println("Do you want to enter the right child for root value"+ node.val+"Y/N??");
        choice=sc.next().charAt(0);
        if(choice=='Y'){
            System.out.println("Enter the right child value");
            int right=sc.nextInt();
            node.right=new CreateNode(right);
            addChild(node.right,sc);
        }
    }

    public static int maxDepth(CreateNode root) {
//        int lDepth;
        if (root == null) {
            return 0;
        }
           int lDepth = maxDepth(root.left);
            int rDepth = maxDepth(root.right);

        return Math.max(lDepth, rDepth)+1;
    }
}

package Hashing;

import java.util.HashSet;

public class HashSetP {
    public static void main(String[] args) {

        HashSet<Integer> myhash = new HashSet<>();

        //add
        myhash.add(2);
        myhash.add(3);
        myhash.add(4);
        myhash.add(5);
        myhash.add(5);

        System.out.println(myhash);

        //search
        if(myhash.contains(2)){
            System.out.println("Preset");
        }

        //delete
        myhash.remove(5);
        System.out.println(myhash);
    }
}

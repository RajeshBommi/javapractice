package GeneralPractice;

public class RepeatedCount {

    public static void main(String[] args) {
        String input = "Rajeshheee";
        int result = Repeat(input);
        System.out.println(result);
    }

    public static int Repeat(String input) {

        int[] count = new int[256];
        for (char i : input.toCharArray()) {
            count[i]++;
        }

        for (char c : input.toCharArray()) {
            if (count[c] > 1) {
                return count[c];
            }
        }
        return -1;
    }
}

//    public static void main(String[] args) {
//        String input = "aabbcddd";
//        int result = countRepeats(input);
//        if (result != -1) {
//            System.out.println("Count of repeated characters: " + result);
//        } else {
//            System.out.println("No repeated characters found.");
//        }
//    }
//
//    public static int countRepeats(String input) {
//        int[] charCount = new int[256]; // Assuming ASCII characters
//
//        // Count occurrences of each character
//        for (char c : input.toCharArray()) {
//            charCount[c]++;
//        }
//
//        // Find the first repeated character and return its count
////        for (char c : input.toCharArray()) {
////            if (charCount[c] > 1) {
////                return charCount[c];
////            }
////        }
//
//        // If no repeated characters found, return -1
//        return -1;
//    }
//}



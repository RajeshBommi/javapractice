package LinkedList;

//import java.util.LinkedList;
//
//public class Sum {
//
//    public static void main(String[] args) {
//        LinkedList<Integer> ll = new LinkedList<>();
//        ll.add(1);
//        ll.add(3);
//        ll.add(5);
//        ll.add(6);
//        ll.add(2);
//        ll.add(7);
//        System.out.println(ll);
//        int n= ll.size();
//        int sum = 0;
//        int sumEven=0;
//        for (Integer integer : ll) {
//            sum = sum + integer;
//        }
//        System.out.println(sum);
//
//        for (int i=0;i<n;i++) {
//            if (ll.get(i) % 2 == 0) {
//                sumEven = sumEven + ll.get(i);
//            }
//            System.out.println(sumEven);
//        }
//    }
//}

public  class Node{
    int data;
    Node next;

    public Node(int data) {
        this.data = data;
        this.next = null;
    }
}

 class LinkedList {
     static Node head;

     //adding nodes
     public void addNode(int data) {
         Node newNode = new Node(data);
         if (head == null) {
             head = newNode;
         } else {
             Node current = head;
             while (current.next != null) {
                 current = current.next;
             }
             current.next = newNode;
         }

     }

     //sum of nodes in linked-list
     public int sum() {
         int totalSum = 0;
         Node current = head;
         while (current != null) {
             totalSum += current.data;
             current = current.next;
         }
         return totalSum;
     }

     //sum of even numbers in linked-list
     public int semEven() {
         int sumEven = 0;
         Node current = head;
         while (current != null) {
             if (current.data % 2 == 0) {
                 sumEven += current.data;
             }
             current = current.next;
         }
         return sumEven;
     }

//     public int count(){
//        int c=0;
//        Node current = head;
//        while(current!=null){
//            c++;
//            current=current.next;
//        }
//        return c;
//     }


     //count of nodes using recursion
     public int count(Node data) {
         if (data == null) {
             return 0;          //time complexity o(n)
             //space complexity o(n)
         }
         return count(data.next) + 1;
     }

     //search element in linked-list
//     public int searchEle(Node data,int key){
//        while(data!=null){
//            if(key== data.data){
//                return data.data;
//
//            }
//            data=data.next;
//        }
//        return 0;
//     }
     public int searchEle(Node data, int key) {
         if (data == null) {
             return 0;
         } else {
             if (key == data.data) {
                 return data.data;
             }
         }
         return searchEle(data.next, key);
     }

     //max element using recursion
     public int maxEle(Node data) {
         if (data == null) {
             return Integer.MIN_VALUE;
         } else {
             int x = maxEle(data.next);
             return Math.max(x, data.data);
//            if(x>data.data){
//                return x;
//            }
//            else {
//                return data.data;
//            }
         }
     }

     //     public int maxEle(Node data){
//        Node current=head;
//        int max= current.data;
//        while (current!=null){
//            if(current.data>max){
//                max= current.data;
//            }
//            current=current.next;
//        }
//        return max;
//     }


     public Node sortLl(Node data) {
         if (data == null || data.next == null) {
             return data; // Base case: return if the list is empty or has only one node
         }

         boolean swapped;
//         Node current;

         do {
             swapped = false;
             data = head;

             while (data.next != null) {
                 if (data.data > data.next.data) {
                     int temp = data.data;
                     data.data = data.next.data;
                     data.next.data = temp;
                     swapped = true;
                 }
                 data = data.next;
             }
//             last = current;
         } while (swapped);

         return head;
     }

     public void removeFirst(){
         if(head==null){
         }
         else if(head.next==null){
             head=null;
         }
         else {
             Node current = head;
             head = head.next;
             current.next = null;
         }

     }
     public void printList() {
         Node current = head;
         while (current != null) {
             System.out.print(current.data + " ");
             current = current.next;
         }
         System.out.println();
     }

public void removeLast(){
         if(head==null){
         }
         else if(head.next==null){
             head=null;
         }
         else if(head.next!=null) {
             Node current = head;
             while (current.next.next != null) {
                 current = current.next;
             }
             current.next = null;
         }
}




     public static void main(String[] args) {
         LinkedList ll = new LinkedList();
         ll.addNode(2);
         ll.addNode(3);
         ll.addNode(4);
         ll.addNode(1);
         ll.addNode(6);
         ll.addNode(9);
         ll.addNode(4);
         ll.addNode(39);

         System.out.println(ll.sum());
         System.out.println(ll.semEven());
         System.out.println(ll.count(head));
         System.out.println(ll.maxEle(head));
         System.out.println(ll.searchEle(head, 111));



         // Print the sorted linked list
         Node current = ll.sortLl(head);
         while (current != null) {
             System.out.print(current.data + " ");
             current = current.next;
         }


         ll.removeFirst();
         ll.printList();
         System.out.println("First node removed.");

         ll.removeLast();
         ll.printList();
         System.out.println("Last node removed.");
     }
 }

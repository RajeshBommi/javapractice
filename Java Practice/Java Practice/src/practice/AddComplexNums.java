package practice;

import java.util.Scanner;

public class AddComplexNums {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter complex  numbers");
        System.out.println("Enter real number a");
        int aReal= sc.nextInt();
        System.out.println("Enter imaginary part");
        int aIma= sc.nextInt();
        System.out.println("Enter the second complex number (a+bi):");
        System.out.println("Enter real number b");
        int bReal= sc.nextInt();
        System.out.println("Enter imaginary part");
        int bIma= sc.nextInt();
        addComplex(aReal,aIma,bReal,bIma);
    }

    public static void addComplex(int aReal ,int aIma,int bReal,int bIma){
        int sumReal= aReal+bReal;
        int sumIma=aIma+bIma;
        System.out.println("The addition of two complex number "+sumReal+"+"+sumIma+"i");
    }
}

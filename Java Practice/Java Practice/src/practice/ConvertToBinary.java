package practice;

import java.util.Scanner;

public class ConvertToBinary {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number to convert");
       int c= sc.nextInt();
       System.out.println("Binary number:");
        convert(c);
    }

    public static void convert(int n){
//        int [] binaryArray= new int[1000];
//        int i=0;
//        while(n>0){
//            binaryArray[i]=n%2;
//            n=n/2;
//            i++;
//        }
//        for(int j=i-1;j>=0;j--){
//             System.out.print(binaryArray[j]);
//        }
        for(int i=32;i>=0;i--){
            int k=(n>>i);
            if((k & 1)>0){
                System.out.print("1");
            }else{
                System.out.print("0");
            }
        }
    }
}

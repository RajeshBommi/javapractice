package practice;

import java.util.Arrays;

public class KthLargest {

    public static void main(String[] args) {

        System.out.println(large(new int[]{3,2,1,5,6,4},2));

    }

    public static int large(int [] arr,int k){
        Arrays.sort(arr);
        return arr[arr.length-k];
    }
}

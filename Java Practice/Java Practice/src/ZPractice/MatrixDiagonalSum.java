package ZPractice;

import java.util.Scanner;

public class MatrixDiagonalSum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of rows");
        int r=sc.nextInt();
        System.out.println("Enter the number of columns");
        int c= sc.nextInt();

        int[][] matrix1= new int[r][c];
        System.out.println("Enter thr rows and columns");

        for (int i=0;i<r;i++){
            for (int j=0;j<c;j++){
                matrix1[i][j]=sc.nextInt();
            }
        }
            sc.close();
        System.out.println("Largest sum :"+sum(matrix1));
    }

    public static int sum(int[][] matrix){
        int upperSum=0;
        int lowerSum=0;

        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix.length;j++){
                if(i+j< matrix.length-1){
                    upperSum+=matrix[i][j];
                } else if (i+j> matrix.length-1) {
                    lowerSum+=matrix[i][j];
                }else {
                    upperSum+=matrix[i][j];
                    lowerSum+=matrix[i][j];
                }
            }
        }

//        for(int i=0;i<matrix.length;i++){
//            for(int j=0;j< matrix[i].length;j++){
//                if(j<=i){
//                    lowerSum+=matrix[i][j];
//                }
//            }
//        }
        System.out.println(upperSum);
        System.out.println(lowerSum);
        return Math.max(upperSum,lowerSum);
    }
}

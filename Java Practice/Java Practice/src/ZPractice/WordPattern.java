package ZPractice;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class WordPattern {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a pattern p:");
        String p= sc.nextLine();
        System.out.println("Enter a String s");
        String s=sc.nextLine();
         boolean res=wordPattern(p,s);
         System.out.println("The result is :"+res);
    }

    public static boolean wordPattern(String p ,String s) {
        String[] arr = s.split(" ");
        if (p.length() != arr.length) {
            return false;
        }
        Map<Character, String> map = new HashMap<>();
        for (int i = 0; i < p.length(); i++) {
            char ch = p.charAt(i);
            boolean containsKey = map.containsKey(ch);
            if(map.containsValue(arr[i]) && !containsKey){
                return false;
            }
            if(containsKey && !map.get(ch).equals(arr[i])){
                return false;
            }
            else{
                map.put(ch,arr[i]);
            }
        }
        return true;
    }
}
